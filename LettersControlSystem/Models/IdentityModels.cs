﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using LettersControlSystem.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LettersControlSystem.Models
{
	// В профиль пользователя можно добавить дополнительные данные, если указать больше свойств для класса ApplicationUser. Подробности см. на странице https://go.microsoft.com/fwlink/?LinkID=317594.
	public class ApplicationUser : IdentityUser
	{
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
		{
			// Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
			// Здесь добавьте утверждения пользователя
			userIdentity.AddClaim(new Claim("OrganizationId", OrganizationId));
			return userIdentity;
		}

		public string OrganizationId { get; set; }
	}

	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public DbSet<Letter> Letters { get; set; }
		public DbSet<Organization> Organizations { get; set; }

		public ApplicationDbContext()
			: base("DefaultConnection", false)
		{
		}

		private static ApplicationDbContext _instance;

		public static ApplicationDbContext Instance => _instance ?? (_instance = Create());

		public static ApplicationDbContext Create()
		{
			return new ApplicationDbContext();
		}
	}
}