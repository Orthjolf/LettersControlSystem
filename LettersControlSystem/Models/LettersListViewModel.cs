﻿using System.Collections.Generic;
using LettersControlSystem.Entity;

namespace LettersControlSystem.Models
{
	public class LettersListViewModel
	{
		public IEnumerable<Letter> Letters { get; set; }

		public string OrgId { get; set; }

		public LettersListViewModel()
		{
			Letters = Letter.Reposirory.GetIncoming();
			OrgId = CurrentUser.OrganizationId;
		}
	}
}