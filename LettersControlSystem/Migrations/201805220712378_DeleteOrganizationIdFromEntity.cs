namespace LettersControlSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteOrganizationIdFromEntity : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Organizations", "OrganizationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Organizations", "OrganizationId", c => c.String());
        }
    }
}
