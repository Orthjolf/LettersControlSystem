namespace LettersControlSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLettersTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Letters",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Number = c.Int(nullable: false),
                        Customer = c.String(),
                        Direction = c.Int(nullable: false),
                        UserId = c.String(),
                        OrganizationId = c.String(),
                        Description = c.String(),
                        UserIdToSendTo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Letters");
        }
    }
}
