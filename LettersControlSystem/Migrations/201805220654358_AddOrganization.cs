namespace LettersControlSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOrganization : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Organizations",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        OrganizationId = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "OrganizationId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "OrganizationId");
            DropTable("dbo.Organizations");
        }
    }
}
