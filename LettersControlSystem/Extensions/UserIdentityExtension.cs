﻿using System.Security.Claims;
using System.Security.Principal;

namespace LettersControlSystem.Extensions
{
	public static class UserIdentityExtension
	{
		public static string GetOrganizationId(this IIdentity identity)
		{
			var claim = ((ClaimsIdentity)identity).FindFirst("OrganizationId");
			return (claim != null) ? claim.Value : string.Empty;
		}
	}
}