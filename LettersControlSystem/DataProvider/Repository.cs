﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LettersControlSystem.Models;

namespace LettersControlSystem.DataProvider
{
	public class Repository<T> where T : Entity.Entity
	{
		protected readonly DbSet<T> DbSet;

		private readonly ApplicationDbContext _db;

		protected Repository()
		{
			_db = ApplicationDbContext.Instance;
			DbSet = _db.Set<T>();
		}

		public void Add(T entity)
		{
			DbSet.Add(entity);
			_db.SaveChanges();
		}

		public T Get(string id)
		{
			return DbSet.First(e => e.Id == id);
		}

		public IReadOnlyCollection<T> GetAll()
		{
			return DbSet.ToList().AsReadOnly();
		}
	}
}