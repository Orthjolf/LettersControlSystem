﻿using System.Collections.Generic;
using System.Linq;
using LettersControlSystem.Entity;
using LettersControlSystem.Enum;

namespace LettersControlSystem.DataProvider
{
	public class LetterReposirory : Repository<Letter>
	{
		public IReadOnlyCollection<Letter> GetIncoming()
		{
			return GetByCurrentOrganizationId().Where(l => l.Direction == Direction.In).ToList().AsReadOnly();
		}

		public IReadOnlyCollection<Letter> GetOutgoing()
		{
			return GetByCurrentOrganizationId().Where(l => l.Direction == Direction.Out).ToList().AsReadOnly();
		}

		public int GetLastNumber()
		{
			var letters = GetByCurrentOrganizationId();
			return !letters.Any() ? 0 : letters.Max(l => l.Number);
		}

		public new IReadOnlyCollection<Letter> GetAll()
		{
			return GetByCurrentOrganizationId().ToList();
		}

		private IReadOnlyCollection<Letter> GetByCurrentOrganizationId()
		{
			return DbSet.Where(l => l.OrganizationId == CurrentUser.OrganizationId).ToList();
		}
	}
}