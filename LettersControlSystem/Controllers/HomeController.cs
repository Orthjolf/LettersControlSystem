﻿using System;
using System.Globalization;
using System.Web.Mvc;
using LettersControlSystem.Entity;
using LettersControlSystem.Enum;
using LettersControlSystem.Models;
using Microsoft.AspNet.Identity;

namespace LettersControlSystem.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View(new LettersListViewModel());
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		[HttpPost]
		public ActionResult AddLetter(string customer, string description, string date)
		{
			Letter.Reposirory.Add(new Letter
			{
				Id = Guid.NewGuid().ToString(),
				Customer = customer,
				Description = description,
				Direction = Direction.In,
				Number = Letter.Reposirory.GetLastNumber() + 1,
				OrganizationId = CurrentUser.OrganizationId,
				UserId = CurrentUser.Id,
				UserIdToSendTo = "qqewq",
				Date = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture)
			});
			return RedirectToAction("Index");
		}
	}
}