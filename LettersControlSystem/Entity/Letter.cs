﻿using System;
using LettersControlSystem.DataProvider;
using LettersControlSystem.Enum;

namespace LettersControlSystem.Entity
{
	public class Letter : Entity
	{
		public static LetterReposirory Reposirory => new Lazy<LetterReposirory>(() => new LetterReposirory()).Value;

		public string OrganizationId { get; set; }

		public int Number { get; set; }

		public string Customer { get; set; }

		public Direction Direction { get; set; }

		public string UserId { get; set; }

		public string Description { get; set; }

		public string UserIdToSendTo { get; set; }

		public DateTime Date { get; set; }
	}
}