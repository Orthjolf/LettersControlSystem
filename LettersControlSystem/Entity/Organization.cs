﻿using System;
using LettersControlSystem.DataProvider;

namespace LettersControlSystem.Entity
{
	public class Organization : Entity
	{
		public static OrganizationRepository Repository =>
			new Lazy<OrganizationRepository>(() => new OrganizationRepository()).Value;

		/// <summary>
		/// Название организации
		/// </summary>
		public string Name { get; set; }
	}
}