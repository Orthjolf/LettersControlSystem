﻿using System;
using System.Security.Principal;
using System.Web;
using LettersControlSystem.Extensions;
using Microsoft.AspNet.Identity;

namespace LettersControlSystem.Entity
{
	public class CurrentUser
	{
		private static readonly IIdentity Identity = HttpContext.Current.User.Identity;

		public static string Id => Identity.GetUserId();

		public static string Name => Identity.GetUserName();

		public static string OrganizationId => Identity.GetOrganizationId();

		public static string OrganizationName => Organization.Repository.Get(OrganizationId).Name;
	}
}